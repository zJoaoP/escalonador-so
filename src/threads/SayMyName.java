package threads;

public class SayMyName implements Runnable{
	private String myName;
	
	public SayMyName(String myName) {
		this.setMyName(myName);
	}

	@Override
	public void run() {
		while(true) {
			System.out.println(myName);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public String getMyName() {
		return myName;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}
	
}