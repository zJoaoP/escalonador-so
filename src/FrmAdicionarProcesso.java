import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import escalonador.Escalonador;
import escalonador.Processo;
import escalonador.Processo.Prioridade;

public class FrmAdicionarProcesso extends JFrame {
	private JLabel lblTitulo;
	private JLabel lblNome;
	private JTextField txtNome;
	private JLabel lblChegada;
	private JLabel lblPrioridade;
	private JLabel lblExec;
	private JLabel lblDeadline;
	private JComboBox <String> comboPrioridade;
	private JButton btnAdicionar;
	private JSpinner spinnerChegada;
	private JSpinner spinnerExecucao;
	private JSpinner spinnerDeadline;
	
	
	private Escalonador escalonador;
	
	public FrmAdicionarProcesso(Escalonador escalonador) {
		this.escalonador = escalonador;
		
		initComponents();
		atribuirFuncoes();
	}
	
	public void atribuirFuncoes() {
		comboPrioridade.addItem("Baixa");
		comboPrioridade.addItem("Normal");
		comboPrioridade.addItem("Alta");
		comboPrioridade.addItem("Tempo Real");
		
		txtNome.setText("Novo Processo");
		
		spinnerChegada.addChangeListener(new ChangeListener() {      
			@Override
			public void stateChanged(ChangeEvent e) {
				if((int) spinnerChegada.getValue() <= -1)
					spinnerChegada.setValue(0);
				if((int) spinnerChegada.getValue() >= 101)
					spinnerChegada.setValue(100);
			}
		});
		
		spinnerExecucao.addChangeListener(new ChangeListener() {      
			@Override
			public void stateChanged(ChangeEvent e) {
				if((int) spinnerExecucao.getValue() <= 1)
					spinnerExecucao.setValue(1);
				if((int) spinnerExecucao.getValue() >= 101)
					spinnerExecucao.setValue(100);
			}
		});
		
		spinnerDeadline.addChangeListener(new ChangeListener() {      
			@Override
			public void stateChanged(ChangeEvent e) {
				if((int) spinnerDeadline.getValue() <= 0)
					spinnerDeadline.setValue(1);
				if((int) spinnerDeadline.getValue() >= 101)
					spinnerDeadline.setValue(100);
			}
		});
		
		spinnerChegada.setValue((int) 0);
		spinnerExecucao.setValue((int) 1);
		spinnerDeadline.setValue((int) 1);
		
		btnAdicionar.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				boolean mesmoNome = false;
				for(Processo p : escalonador.getProcessos()) {
					if(p.getNome().equals(txtNome.getText())) {
						mesmoNome = true;
						break;
					}
				}
				if(mesmoNome)
					JOptionPane.showMessageDialog(null, "Já existe um processo com este nome. Tente outro.", "Nome Inválido!", JOptionPane.INFORMATION_MESSAGE); 
				else if(escalonador.getProcessos().size() >= 15)
					JOptionPane.showMessageDialog(null, "Já existem processos demais na lista de processos.", "Processos Demais!", JOptionPane.INFORMATION_MESSAGE); 
				else {
					Prioridade prioridade;
					switch(comboPrioridade.getSelectedItem().toString()) {
						case "Baixa":
							prioridade = Prioridade.BAIXA;
							break;
						case "Normal":
							prioridade = Prioridade.NORMAL;
							break;
						case "Alta":
							prioridade = Prioridade.ALTA;
							break;
						default:
							prioridade = Prioridade.TEMPO_REAL;
					}
					Processo p = new Processo(txtNome.getText(),
							(int) spinnerChegada.getValue(),
							(int) spinnerExecucao.getValue(), 
							prioridade, 
							(int) spinnerDeadline.getValue());
					
					escalonador.adicionarProcesso(p);
					fecharJanela();
				}
			} 
		});
	}
	
	private void fecharJanela() {
		this.dispose();
	}

	private void initComponents() {
		lblTitulo = new JLabel();
		lblNome = new JLabel();
		txtNome = new JTextField();
		lblChegada = new JLabel();
		lblPrioridade = new JLabel();
		spinnerChegada = new JSpinner();
		lblExec = new JLabel();
		spinnerExecucao = new JSpinner();
		lblDeadline = new JLabel();
		comboPrioridade = new JComboBox <String> ();
		spinnerDeadline = new JSpinner();
		btnAdicionar = new JButton();

		Container contentPane = getContentPane();

		lblTitulo.setText("Adicionar Processo");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Dialog", Font.BOLD, 20));

		lblNome.setText("Nome:");

		lblChegada.setText("Chegada:");

		lblPrioridade.setText("Prioridade:");

		lblExec.setText("Execução:");

		lblDeadline.setText("Deadline:");

		btnAdicionar.setText("Adicionar Novo Processo");

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addComponent(lblTitulo, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGroup(contentPaneLayout.createParallelGroup()
								.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
									.addComponent(lblExec, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblChegada, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(lblNome)
								.addComponent(lblDeadline))
							.addGap(14, 14, 14)
							.addGroup(contentPaneLayout.createParallelGroup()
								.addComponent(txtNome, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
								.addGroup(contentPaneLayout.createSequentialGroup()
									.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addComponent(spinnerChegada, GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
										.addComponent(spinnerExecucao, GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
										.addComponent(spinnerDeadline, GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
									.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(contentPaneLayout.createParallelGroup()
										.addGroup(contentPaneLayout.createSequentialGroup()
											.addComponent(lblPrioridade)
											.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(comboPrioridade, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))
										.addComponent(btnAdicionar, GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE))))))
					.addContainerGap())
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblTitulo, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(lblChegada)
						.addComponent(spinnerChegada, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPrioridade)
						.addComponent(comboPrioridade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(lblExec)
								.addComponent(spinnerExecucao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(lblDeadline)
								.addComponent(spinnerDeadline, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(btnAdicionar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(11, Short.MAX_VALUE))
		);
		pack();
		setResizable(false);
		setLocationRelativeTo(getOwner());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}