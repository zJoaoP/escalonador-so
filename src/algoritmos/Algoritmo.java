package algoritmos;

import java.util.ArrayList;
import escalonador.Processo;

public abstract class Algoritmo{
	
	private ArrayList <Processo> processos = new ArrayList <Processo> ();
	private Processo processoAtual;
	private int tempoAtual;
	
	public Algoritmo(){}

	public Algoritmo(ArrayList <Processo> processos){
		this.setProcessos(processos);
	}

	public ArrayList <Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(ArrayList <Processo> processos) {
		this.processos = processos;
	}

	public Processo getProcessoAtual() {
		return processoAtual;
	}

	public void setProcessoAtual(Processo processoAtual) {
		this.processoAtual = processoAtual;
	}

	public int getTempoAtual() {
		return tempoAtual;
	}

	public void setTempoAtual(int tempoAtual) {
		this.tempoAtual = tempoAtual;
	}
}
