package algoritmos;

import escalonador.Processo;

public class FIFO extends NaoPreemptivo{
	@Override
	public boolean compararProcessos(Processo a, Processo b) {
		return a.getTempoChegada() < b.getTempoChegada();
	}	
}
