package algoritmos;

import escalonador.Processo;

public class Prioridades extends Preemptivo{
	@Override
	public boolean compararProcessos(Processo a, Processo b) {
		return b.getPrioridade().menorQue(a.getPrioridade());
	}	
}