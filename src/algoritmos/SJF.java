package algoritmos;

import escalonador.Processo;

public class SJF extends NaoPreemptivo{
	@Override
	public boolean compararProcessos(Processo a, Processo b) {
		return a.getTempoExecucao() < b.getTempoExecucao();
	}	
	
}