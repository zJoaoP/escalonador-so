package algoritmos;

import escalonador.Processo;

public class EDF extends Preemptivo{
	@Override
	public boolean compararProcessos(Processo a, Processo b) {
		int deadLineA = (a.getTempoChegada() + a.getDeadLine()) - super.getTempoAtual(); 
		int deadLineB = (b.getTempoChegada() + b.getDeadLine()) - super.getTempoAtual();
		return (deadLineA < deadLineB);
	}	
}