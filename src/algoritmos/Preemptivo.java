package algoritmos;

import java.util.ArrayList;

import escalonador.Processo;
import timeline.Intervalo;
import timeline.TimeLine;

public abstract class Preemptivo extends Algoritmo{	
	private ArrayList <Processo> fila = new ArrayList <Processo> ();
	private int[] tempoRestante;
	
	public abstract boolean compararProcessos(Processo a, Processo b);

	public TimeLine gerarTimeLine(int quantum, int sobrecarga){	
		ArrayList <Processo> finalizados = new ArrayList <Processo> ();
		tempoRestante = new int[this.getProcessos().size()];
		
		for(Processo p : this.getProcessos()) //Inicializando o array de tempos restantes.
			setTempoRestante(p, p.getTempoExecucao());
		
		TimeLine t = new TimeLine();
		
		int tempoInicio = 0, execucaoAtual = 0;
		
		while(this.getProcessos().size() > finalizados.size()){
			if(execucaoAtual > 0) execucaoAtual--;
			for(Processo p : this.getProcessos()){
				if(p.getTempoChegada() != getTempoAtual())
					continue;
				else
					fila.add(p);
			}
			if(execucaoAtual == 0 && getProcessoAtual() != null){//Finalizando o processo.
				if(getTempoAtual() - tempoInicio > quantum) {
					t.adicionarIntervalo(new Intervalo(getProcessoAtual(), tempoInicio, getTempoAtual(), true));
					setTempoRestante(getProcessoAtual(), getTempoRestante(getProcessoAtual()) - quantum);
					fila.add(getProcessoAtual());
				}	
				else {
					t.adicionarIntervalo(new Intervalo(getProcessoAtual(), tempoInicio, getTempoAtual(), false));
					setTempoRestante(getProcessoAtual(), 0);
					finalizados.add(getProcessoAtual());
					getProcessoAtual().pararThread();
				}
				setProcessoAtual(null);
			}
			if(!fila.isEmpty() && execucaoAtual == 0) {
				for(Processo p : fila){
					if(getProcessoAtual() == null)
						setProcessoAtual(p);
					else if(compararProcessos(p, getProcessoAtual()))
						setProcessoAtual(p);
					else
						continue;
				}
				fila.remove(getProcessoAtual());
				
				tempoInicio = getTempoAtual();
				execucaoAtual = (getTempoRestante(getProcessoAtual()) <= quantum) ? getTempoRestante(getProcessoAtual()) : quantum + sobrecarga;
				
				getProcessoAtual().executarThread();
				try {
					int tempoProcessamento = (execucaoAtual > quantum) ? quantum : getTempoRestante(getProcessoAtual());
					Thread.sleep(10*tempoProcessamento);	
				}
				catch(Exception e){
					System.out.println("Exception no bloco principal: " + e.getMessage());
				}				
				//Um Thread.sleep(10*sobrecarga) poderia ser inserido aqui, para simular o tempo de chaveamento entre processos.
				//No entanto, é realmente necessário fazer isso?
				getProcessoAtual().interromperThread();
			}
			setTempoAtual(getTempoAtual() + 1);
		}
		return t;
	}
	
	private void setTempoRestante(Processo p, int tempoExecucao) {
		tempoRestante[this.getProcessos().indexOf(p)] = tempoExecucao;
	}

	private int getTempoRestante(Processo p) {
		return tempoRestante[this.getProcessos().indexOf(p)];
	}
	
	public ArrayList <Processo> getFila(){
		return this.fila;
	}
}