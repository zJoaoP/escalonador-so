package algoritmos;

import java.util.ArrayList;

import escalonador.Processo;
import timeline.Intervalo;
import timeline.TimeLine;

public abstract class NaoPreemptivo extends Algoritmo{	
	public abstract boolean compararProcessos(Processo a, Processo b);

	public TimeLine gerarTimeLine() {
		ArrayList <Processo> fila = new ArrayList <Processo> ();
		ArrayList <Processo> finalizados = new ArrayList <Processo> ();
		
		TimeLine t = new TimeLine();
		
		int tempoInicio = 0, execucaoAtual = 0;
		
		while(this.getProcessos().size() > finalizados.size()){
			if(execucaoAtual > 0) execucaoAtual--;
			for(Processo p : this.getProcessos()){
				if(p.getTempoChegada() != getTempoAtual())
					continue;
				else
					fila.add(p);
			}
			if(execucaoAtual == 0 && getProcessoAtual() != null){//Finalizando o processo.
				t.adicionarIntervalo(new Intervalo(getProcessoAtual(), tempoInicio, getTempoAtual(), false));	
				finalizados.add(getProcessoAtual());		
				setProcessoAtual(null);
			}
			if(!fila.isEmpty() && execucaoAtual == 0) {				
				for(Processo p : fila){
					if(getProcessoAtual() == null)
						setProcessoAtual(p);
					else if(compararProcessos(p, getProcessoAtual()))
						setProcessoAtual(p);
					else
						continue;
				}
				fila.remove(getProcessoAtual());

				tempoInicio = getTempoAtual();
				execucaoAtual = getProcessoAtual().getTempoExecucao();
				
				getProcessoAtual().executarThread();
				try {
					Thread.sleep(10*execucaoAtual);	
				}
				catch(Exception e){
					System.out.println("Exception no bloco principal: " + e.getMessage());
				}				
				getProcessoAtual().pararThread();
			}
			setTempoAtual(getTempoAtual() + 1);
		}	
		return t;
	}
}
	
	
	
	