package timeline;

import java.util.ArrayList;

import escalonador.Processo;

public class TimeLine {
	private ArrayList <Intervalo> intervalos = new ArrayList <Intervalo> ();
	
	public TimeLine(){}
	
	public void adicionarIntervalo(Intervalo i){
		intervalos.add(i);
	}
	
	public String toString(){
		String str = "";
		for(Intervalo i : intervalos){
			str += i.toString();
		}
		return str + "\n";
	}

	public double calcularTurnAround() {
		double turnAround = 0.0;
		ArrayList <Processo> procs = new ArrayList <Processo> ();
		for(Intervalo i : intervalos) {
			Processo p = i.getProcesso();
			if(procs.indexOf(p) != -1) continue;
			
			procs.add(p);
			Intervalo ultimoIntervalo = null;
			
			for(Intervalo busca : intervalos)
				if(busca.getProcesso().equals(p)) ultimoIntervalo = busca;
	
			turnAround += ultimoIntervalo.getFim() - p.getTempoChegada();			
		}		
		return (procs.size() == 0) ? 0.0 : turnAround/procs.size();
	}
	
	public int getSize() {
		return intervalos.size();
	}
	
	public ArrayList <Intervalo> getIntervalos(){
		return this.intervalos;
	}
}
