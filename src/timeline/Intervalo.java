package timeline;

import escalonador.Processo;

public class Intervalo {
	private int inicio;
	private int fim;
	private boolean sobrecarga;
	private Processo processo;
	
	public Intervalo(Processo processo, int inicio, int fim, boolean sobrecarga){
		this.setProcesso(processo);
		this.setInicio(inicio);
		this.setFim(fim);
		this.setSobrecarga(sobrecarga);
	}
	
	public String toString(){
		String str = this.getProcesso().getNome() + " " + this.getInicio() + " " + this.getFim();
		return str + "\n";
	}

	public int getInicio() {
		return inicio;
	}
	public void setInicio(int inicio) {
		this.inicio = inicio;
	}
	public int getFim() {
		return fim;
	}
	public void setFim(int fim) {
		this.fim = fim;
	}
	public Processo getProcesso() {
		return processo;
	}
	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public boolean teveSobrecarga() {
		return sobrecarga;
	}

	public void setSobrecarga(boolean sobrecarga) {
		this.sobrecarga = sobrecarga;
	}
}
