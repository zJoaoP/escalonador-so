package timeline;

import java.awt.Color;
import java.awt.Paint;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.GanttRenderer;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.SimpleTimePeriod;

import escalonador.Processo;

public class Gantt{	
	public class MyGanttRenderer extends GanttRenderer {
		private int column = 0;
		private int count = 0;
		
        public MyGanttRenderer() {
            super();
        }

        public Paint getItemPaint(int row, int column) {
        	if(this.column != column) {
        		this.column = column;
        		count = 0;
        	}
        	
        	int acumulador = count % 4;
        	count++;
        	
        	if(acumulador < 2) 
        		return Color.RED;
        	else
        		return Color.blue;
        }
    }
	
	private TimeLine timeLine;
	private ChartPanel chartPanel;
	private String titulo;
	private TaskSeriesCollection collection;
	private int quantum;
	private int sobrecarga;
	
	public Gantt(String titulo, TimeLine timeLine, int quantum, int sobrecarga) {
		this.setTimeLine(timeLine);
		this.setTitulo(titulo);
		this.setSobrecarga(sobrecarga);
		this.setQuantum(quantum);
		gerarGrafico();
	}
	
	public IntervalCategoryDataset createDataset() {//TODO: Podem existir itens com mesmo nome, mesmo inicio e mesmo fim. Estes são vistos como itens iguais e não são plotados no gráfico. BUG
        collection = new TaskSeriesCollection();
        if(timeLine.calcularTurnAround() == 0) return collection;
        else {        	
        	ArrayList <Processo> processos = new ArrayList <Processo> ();
        	TaskSeries tempoExecucao = new TaskSeries("Tempo de Execucao");        	
        	for(Intervalo i : timeLine.getIntervalos()) {
        		if(processos.indexOf(i.getProcesso()) != -1) continue;
        		
        		processos.add(i.getProcesso());
        		int tempoInicial = i.getInicio();
        		int tempoFinal = -1;
        		
        		Task t = new Task(i.getProcesso().getNome(), new SimpleTimePeriod((long) tempoInicial, (long) tempoInicial));
        		for(Intervalo aux : timeLine.getIntervalos()) {
        			if(aux.getProcesso() == i.getProcesso()) {
        				tempoFinal = aux.getFim();
        				if(aux.teveSobrecarga()) {
        					Task tarefaQuantum = new Task("quantum", new SimpleTimePeriod((long) aux.getInicio(), (long) aux.getFim() - this.getSobrecarga()));
        					Task tarefaSobrecarga = new Task("sobrecarga", new SimpleTimePeriod((long) aux.getFim() - this.getSobrecarga(), (long) aux.getFim()));
            				t.addSubtask(tarefaQuantum);
            				t.addSubtask(tarefaSobrecarga);
        				}
        				else {
        					Task sub = new Task("quantum", new SimpleTimePeriod((long) aux.getInicio(), (long) aux.getFim()));
            				t.addSubtask(sub);
        				}
        			}
        		}
        		t.setDuration(new SimpleTimePeriod((long) tempoInicial, (long) tempoFinal));
        		tempoExecucao.add(t);
        	}
        	collection.add(tempoExecucao);          	
        	return collection;
        }
    }
	
	private JFreeChart createChart(final IntervalCategoryDataset dataset) {
        final JFreeChart chart = ChartFactory.createGanttChart(
            this.getTitulo(),
            "Processos",              // domain axis label
            "Tempo",              // range axis label
            dataset,             // data
            false,                // include legend
            false,                // tooltips
            false                // urls
        );
        return chart;    
    }
	
	public void gerarGrafico() {
		IntervalCategoryDataset dataSet = createDataset();
		JFreeChart chart = createChart(dataSet);
		
		CategoryPlot plot = chart.getCategoryPlot();
		DateAxis axis = (DateAxis) plot.getRangeAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("S"));
		axis.setAutoRangeMinimumSize(5);
		
		if(this.getTimeLine().getIntervalos().size() == 0)
			axis.setRange(0, 1);
		else {
			int size = this.getTimeLine().getIntervalos().size();
			axis.setRange(0, this.getTimeLine().getIntervalos().get(size - 1).getFim());
		}
		MyGanttRenderer renderer = new MyGanttRenderer();
		plot.setRenderer(renderer);
		
		this.chartPanel = new ChartPanel(chart);
		this.chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
	}

	public TimeLine getTimeLine() {
		return timeLine;
	}
	
	public void setTimeLine(TimeLine timeLine) {
		this.timeLine = timeLine;
	}
	
	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}
	public ChartPanel getChartPanel() {
		return this.chartPanel;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getQuantum() {
		return quantum;
	}

	public void setQuantum(int quantum) {
		this.quantum = quantum;
	}

	public int getSobrecarga() {
		return sobrecarga;
	}

	public void setSobrecarga(int sobrecarga) {
		this.sobrecarga = sobrecarga;
	}
}