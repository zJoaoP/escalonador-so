package escalonador;

import threads.SayMyName;

public class Processo {
	public enum Prioridade{
		BAIXA(0), NORMAL(1), ALTA(2), TEMPO_REAL(3);
		
		private int prioridade;
		Prioridade(int prioridade){
			this.prioridade = prioridade;
		}
		
		public boolean menorQue(Prioridade p) {
			return p.prioridade > this.prioridade;
		}
	};
	
	private String nome;
	private int tempoChegada;
	private int tempoExecucao;
	private int deadLine;
	private Prioridade prioridade;
	
	private Thread thread;
	
	public Processo(String nome, int tempoChegada, int tempoExecucao){
		this.setNome(nome);
		this.setTempoChegada(tempoChegada);
		this.setTempoExecucao(tempoExecucao);
		this.setDeadLine(0);
		this.setPrioridade(Prioridade.BAIXA);
	}
	public Processo(String nome, int tempoChegada, int tempoExecucao, int deadLine){
		this(nome, tempoChegada, tempoExecucao);
		this.setDeadLine(deadLine);
	}
	public Processo(String nome, int tempoChegada, int tempoExecucao, Prioridade prioridade){
		this(nome, tempoChegada, tempoExecucao);
		this.setPrioridade(prioridade);
	}
	public Processo(String nome, int tempoChegada, int tempoExecucao, Prioridade prioridade, int deadLine){
		this(nome, tempoChegada, tempoExecucao, prioridade);
		this.setDeadLine(deadLine);
	}

	public int getTempoChegada() {
		return tempoChegada;
	}

	public void setTempoChegada(int tempoChegada) {
		this.tempoChegada = tempoChegada;
	}

	public int getTempoExecucao() {
		return tempoExecucao;
	}

	public void setTempoExecucao(int tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}

	public int getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(int deadLine) {
		this.deadLine = deadLine;
	}

	public Prioridade getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString(){
		String str = nome + " " + tempoChegada + " " + tempoExecucao + " " + prioridade + " " + deadLine;
		return str + "\n";
	}
	
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
	@SuppressWarnings("deprecation")
	public void executarThread() {
		if(this.getThread() == null) {
			this.setThread(new Thread(new SayMyName(nome)));
			this.getThread().start();
		}
		else
			this.getThread().resume();
	}
	
	@SuppressWarnings("deprecation")
	public void interromperThread() {
		this.getThread().suspend();
	}
	
	@SuppressWarnings("deprecation")
	public void pararThread() {
		this.getThread().stop();
		this.setThread(null);
	}
}
