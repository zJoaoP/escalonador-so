package escalonador;

import java.util.ArrayList;

import algoritmos.Algoritmo;
import algoritmos.NaoPreemptivo;
import algoritmos.Preemptivo;
import timeline.TimeLine;

public class Escalonador {
	private ArrayList <Processo> processos = new ArrayList <Processo> ();
	private int sobrecarga;
	private int quantum;
	private TimeLine timeLine;
	
	public Escalonador (ArrayList <Processo> processos, int sobrecarga, int quantum){
		this(sobrecarga, quantum);
		this.processos = processos;
	}
	
	public Escalonador(int sobrecarga, int quantum) {
		this.setSobrecarga(sobrecarga);
		this.setQuantum(quantum);
		this.setTimeLine(new TimeLine());
	}
	
	public Escalonador() { //Valores padrão de sobrecarga e quantum.
		this.setSobrecarga(1);
		this.setQuantum(2);
	}

	public void adicionarProcesso(Processo p){
		processos.add(p);
	}
	
	public void removerProcesso(Processo p){
		processos.remove(p);
	}
	
	public ArrayList <Processo> getProcessos(){
		return this.processos;
	}
	
	public void aplicarAlgoritmo(Algoritmo algoritmo){
		TimeLine timeLine = null;
		algoritmo.setProcessos(this.getProcessos());
		
		if(algoritmo instanceof NaoPreemptivo)
			timeLine = ((NaoPreemptivo) algoritmo).gerarTimeLine();
		else 
			timeLine = ((Preemptivo) algoritmo).gerarTimeLine(this.getQuantum(), this.getSobrecarga());
		
		this.setTimeLine(timeLine);
	}

	public int getSobrecarga() {
		return sobrecarga;
	}

	public void setSobrecarga(int sobrecarga) {
		this.sobrecarga = sobrecarga;
	}

	public int getQuantum() {
		return quantum;
	}

	public void setQuantum(int quantum) {
		this.quantum = quantum;
	}

	public TimeLine getTimeLine() {
		return timeLine;
	}

	public void setTimeLine(TimeLine timeLine) {
		this.timeLine = timeLine;
	}
}
