import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.jfree.chart.ChartPanel;
import org.jfree.ui.tabbedui.VerticalLayout;

import algoritmos.EDF;
import algoritmos.FIFO;
import algoritmos.Prioridades;
import algoritmos.RoundRobin;
import algoritmos.SJF;
import escalonador.Escalonador;
import escalonador.Processo;
import timeline.Gantt;
import timeline.TimeLine;

public class App extends JFrame{
	private JPanel panel1;
	private JScrollPane tblProcessos;
	private JTable table1;
	private JButton btnAdicionarProcesso;
	private JButton btnRemoverProcesso;
	private JLabel lblTituloTrabalho;
	private JButton btnGerarGantt;
	private JLabel lblAlgoritmo;
	private JComboBox <String> comboAlgoritmo;
	private JLabel lblQuantum;
	private JSpinner spinnerQuantum;
	private JLabel lblSobrecarga;
	private JSpinner spinnerSobrecarga;
	
	private ChartPanel panelGantt;
	private Escalonador escalonador;
	private DefaultTableModel modeloTabela;
	
	public static void main(String args[]) {
		new App();
	}
	
	public App() {
		initComponents();
		atribuirFuncoes();
	}
	
	private void atribuirFuncoes() {	
		comboAlgoritmo.addItem("FIFO");
		comboAlgoritmo.addItem("SJF");
		comboAlgoritmo.addItem("Round Robin");
		comboAlgoritmo.addItem("EDF");
		comboAlgoritmo.addItem("Prioridades");
		
		btnAdicionarProcesso.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				new FrmAdicionarProcesso(escalonador).setVisible(true);
			}
		});
		
		btnRemoverProcesso.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				if(table1.getSelectedRow() == -1) return;
				Object nomeProcesso = table1.getValueAt(table1.getSelectedRow(), 0);
				Processo alvo = null;
				for(Processo p : escalonador.getProcessos()) {
					if(p.getNome().equals(nomeProcesso)) {
						alvo = p;
						break;
					}
				}
				escalonador.removerProcesso(alvo);
				modeloTabela.removeRow(table1.getSelectedRow());
			}
		});
		
		btnGerarGantt.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				escalonador.setTimeLine(new TimeLine());
				
				Container contentPane = getContentPane();
				contentPane.remove(panelGantt);
				
				String str = comboAlgoritmo.getItemAt(comboAlgoritmo.getSelectedIndex());

				escalonador.setQuantum((int) spinnerQuantum.getValue());
				escalonador.setSobrecarga((int) spinnerSobrecarga.getValue());
				
				if(str.equals("FIFO")) escalonador.aplicarAlgoritmo(new FIFO());
				else if(str.equals("SJF")) escalonador.aplicarAlgoritmo(new SJF());
				else if(str.equals("Round Robin")) escalonador.aplicarAlgoritmo(new RoundRobin());
				else if(str.equals("EDF")) escalonador.aplicarAlgoritmo(new EDF());
 				else escalonador.aplicarAlgoritmo(new Prioridades());
				
				Gantt g = new Gantt(comboAlgoritmo.getItemAt(comboAlgoritmo.getSelectedIndex()) + " (TurnAround: " + 
									escalonador.getTimeLine().calcularTurnAround() + ")",
									escalonador.getTimeLine(),
									escalonador.getQuantum(),
									escalonador.getSobrecarga());
				
				panelGantt = g.getChartPanel();
				contentPane.add(panelGantt);
				contentPane.invalidate();
				contentPane.revalidate();
				contentPane.repaint();
				
				JOptionPane.showMessageDialog(null, "Turn around: " + escalonador.getTimeLine().calcularTurnAround(), "TurnAround", JOptionPane.INFORMATION_MESSAGE); 
			} 
		});
		
		spinnerQuantum.addChangeListener(new ChangeListener() {      
			@Override
			public void stateChanged(ChangeEvent e) {
				if((int) spinnerQuantum.getValue() < 1) spinnerQuantum.setValue(1);
				if((int) spinnerQuantum.getValue() > 20) spinnerQuantum.setValue(20);
			}
		});
		
		spinnerSobrecarga.addChangeListener(new ChangeListener() {      
			@Override
			public void stateChanged(ChangeEvent e) {
				if((int) spinnerSobrecarga.getValue() < 1) spinnerSobrecarga.setValue(1);
				if((int) spinnerSobrecarga.getValue() > 10) spinnerSobrecarga.setValue(10);
			}
		});
		
		this.addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				while(modeloTabela.getRowCount() > 0) {
					modeloTabela.removeRow(0);
				}
				for(Processo p : escalonador.getProcessos()) {
					Vector <String> novoProcesso = new Vector <String> ();
					novoProcesso.add(p.getNome());
					novoProcesso.add(Integer.toString(p.getTempoChegada()));
					novoProcesso.add(Integer.toString(p.getTempoExecucao()));
					novoProcesso.add(Integer.toString(p.getDeadLine()));
					novoProcesso.add(p.getPrioridade().toString());
					
					modeloTabela.addRow(novoProcesso);
				}
			}
	    });
	}

	private void initComponents() {
		panel1 = new JPanel();
		tblProcessos = new JScrollPane();
		btnAdicionarProcesso = new JButton();
		btnRemoverProcesso = new JButton();
		lblTituloTrabalho = new JLabel();
		btnGerarGantt = new JButton();
		lblAlgoritmo = new JLabel();
		comboAlgoritmo = new JComboBox <String> ();
		lblQuantum = new JLabel();
		spinnerQuantum = new JSpinner();
		lblSobrecarga = new JLabel();
		spinnerSobrecarga = new JSpinner();
		
		table1 = new JTable() {
			DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();	
			@Override
		    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
				renderRight.setHorizontalAlignment(SwingConstants.CENTER);
		        return renderRight;
		    }
		};
		
		//======== this ========
		Container contentPane = getContentPane();
		contentPane.setLayout(new VerticalLayout());
		
		String [] colunas = {"Nome", "Inicio", "Tempo", "Dead Line", "Prioridade"};
		modeloTabela = new DefaultTableModel() {
			@Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		modeloTabela.setColumnIdentifiers(colunas);
		table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table1.setModel(modeloTabela);

		//======== panel1 ========
		{
			panel1.setPreferredSize(new Dimension(1018, 200));
			//======== tblProcessos ========
			{
				tblProcessos.setViewportView(table1);
			}

			//---- btnAdicionarProcesso ----
			btnAdicionarProcesso.setText("Adicionar Processo");

			//---- btnRemoverProcesso ----
			btnRemoverProcesso.setText("Remover Processo");

			//---- lblTituloTrabalho ----
			lblTituloTrabalho.setText("Escalonador de Processos (CC 2017.1)");
			lblTituloTrabalho.setHorizontalAlignment(SwingConstants.CENTER);
			lblTituloTrabalho.setFont(new Font("Dialog", Font.BOLD, 16));

			//---- btnGerarGantt ----
			btnGerarGantt.setText("Gerar Gr\u00e1fico de Gantt");

			//---- lblAlgoritmo ----
			lblAlgoritmo.setText("Algoritmo:");

			//---- lblQuantum ----
			lblQuantum.setText("Quantum:");

			//---- lblSobrecarga ----
			lblSobrecarga.setText("Sobrecarga");

			GroupLayout panel1Layout = new GroupLayout(panel1);
			panel1.setLayout(panel1Layout);
			panel1Layout.setHorizontalGroup(
				panel1Layout.createParallelGroup()
					.addGroup(panel1Layout.createSequentialGroup()
						.addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
							.addComponent(tblProcessos, GroupLayout.PREFERRED_SIZE, 433, GroupLayout.PREFERRED_SIZE)
							.addGroup(panel1Layout.createSequentialGroup()
								.addComponent(btnAdicionarProcesso, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(btnRemoverProcesso, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(panel1Layout.createParallelGroup()
							.addComponent(lblTituloTrabalho, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
							.addComponent(btnGerarGantt, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
							.addGroup(panel1Layout.createSequentialGroup()
								.addComponent(lblAlgoritmo)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(comboAlgoritmo, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE))
							.addGroup(panel1Layout.createSequentialGroup()
								.addComponent(lblQuantum)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(spinnerQuantum, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
								.addComponent(lblSobrecarga)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(spinnerSobrecarga, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
			);
			panel1Layout.setVerticalGroup(
				panel1Layout.createParallelGroup()
					.addGroup(panel1Layout.createSequentialGroup()
						.addGroup(panel1Layout.createParallelGroup()
							.addComponent(tblProcessos, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
							.addGroup(panel1Layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(lblTituloTrabalho, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
									.addComponent(lblAlgoritmo)
									.addComponent(comboAlgoritmo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
									.addComponent(lblQuantum)
									.addComponent(spinnerQuantum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(spinnerSobrecarga, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblSobrecarga))))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(panel1Layout.createParallelGroup()
							.addComponent(btnAdicionarProcesso, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(panel1Layout.createSequentialGroup()
								.addComponent(btnRemoverProcesso, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE))
							.addComponent(btnGerarGantt, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap())
			);
		}
		contentPane.add(panel1);
		
		escalonador = new Escalonador(1, 2);
		
		Gantt g = new Gantt("Gráfico de Escalonamento", new TimeLine(), escalonador.getQuantum(), escalonador.getSobrecarga());
		panelGantt = g.getChartPanel();
		contentPane.add(panelGantt);
		
		spinnerQuantum.setValue((int) escalonador.getQuantum());
		spinnerSobrecarga.setValue((int) escalonador.getSobrecarga());
		
		pack();
		setLocationRelativeTo(getOwner());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Gerador de gráficos de escalonamento");
		setVisible(true);
	}
}